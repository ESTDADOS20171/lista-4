package hashestendivel

class Balde[E <% Ordered[E]: ClassManifest](capacidade: Int, h: E => Int, altura: Int = 0, default: E) {

  private var elementos = new Array[E](0);

  private var esq: Balde[E] = null; // bit 0
  private var dir: Balde[E] = null; // bit 1

  private var hash: E => Int = h;

  if (altura == 0) {
    esq = new Balde[E](capacidade, h, altura + 1, default);
    dir = new Balde[E](capacidade, h, altura + 1, default);
    elementos = null;
  }

  private def tamanho(): Int = if (elementos != null) elementos.length else 0;

  def inserir(e: E) {

    if (elementos == null) {

      var h = hash(e) >> altura;

      if ((h & 1) == 1) {

        if (dir.tamanho >= capacidade) {
          dir.dividir();
        }
        dir.inserir(e);

      } else {
        if (esq.tamanho >= capacidade) {
          esq.dividir();
        }
        esq.inserir(e);
      }

    } else {

      if (tamanho >= capacidade) {
        dividir();
        //inserir(e);
      } else {
        elementos = elementos :+ e;
      }
    }
  }

  //def tamanho(): Int = if (elementos != null) elementos.length else (esq.tamanho + dir.tamanho);

  //private def estaCheio() = (elementos != null && quantidade == elementos.length);

  def contem(e: E, altura: Int = 0): Boolean = {
    if (elementos != null) {
      return elementos.contains(e);
    }

    var h = hash(e) >> altura;

    if ((h & 1) == 1) {
      return dir.contem(e, altura + 1);
    } else {
      return esq.contem(e, altura + 1);
    }

  }

  private def dividir() {
    esq = new Balde[E](capacidade, h, altura + 1, default);
    dir = new Balde[E](capacidade, h, altura + 1, default);

    if (elementos != null) {
      elementos.map { e =>

        var h = hash(e) >> (altura);
        if ((h & 1) == 1) {
          dir.inserir(e);
        } else {
          esq.inserir(e);
        }
      };
    }

    elementos = null;
  }

  def remover(e: E, altura: Int = 0): Boolean = {

    var t = tamanho();
    if (elementos != null) {
      elementos = elementos.filter { el => e != el };
      return elementos.length == t;
    }

    var h = hash(e) >> altura;

    if ((h & 1) == 1) dir.remover(e, altura + 1) else esq.remover(e, altura + 1);
  }

  override def toString = if (elementos != null) elementos.mkString("(", ", ", ")") else "E[" + altura + "]" + esq.toString + " D[" + altura + "]" + dir.toString;

}