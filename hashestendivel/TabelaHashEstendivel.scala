package hashestendivel

class TabelaHashEstendivel[E <% Ordered[E]: ClassManifest](capacidade: Int = 4, default: E) {
  private var balde = new Balde[E](capacidade, this.h, 0, default);

  //def tamanho() = balde.tamanho();

  def inserir(valor: E) = balde.inserir(valor);

  def buscar(valor: E) = balde.contem(valor);

  def remover(valor: E) = balde.remover(valor);

  // minha funcao de hash
  private def h(valor: E): Int = valor.hashCode();

  override  def toString = balde.toString();
  
  def liberar() {
    balde = null;
  }

}