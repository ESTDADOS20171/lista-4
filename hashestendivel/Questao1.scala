package hashestendivel

object Questao1 extends App {

  var tb = new TabelaHashEstendivel[Int](2, 0);

  println("Tabela Hash: \n" + tb + "\n");

  println("Inserir 1.");
  tb.inserir(1);
  println("Tabela Hash: \n" + tb + "\n");

  println("Inserir 3.");
  tb.inserir(3);
  println("Tabela Hash: \n" + tb + "\n");

  println("Inserir 5.");
  tb.inserir(5);
  println("Tabela Hash: \n" + tb + "\n");

  println("Inserir 7.");
  tb.inserir(7);
  println("Tabela Hash: \n" + tb + "\n");

  println("Inserir 13.");
  tb.inserir(13);
  println("Tabela Hash: \n" + tb);

  println("Inserir -4.");
  tb.inserir(-4);
  println("Tabela Hash: \n" + tb);
  
    println("Tem 10? " + tb.buscar(10));
    println("Tem 11? " + tb.buscar(13));
    println("Tem 4? " + tb.buscar(4));
    println("Tem -4? " + tb.buscar(-4));
  
    println("\nTabela Hash: \n" + tb);
    println("Remover 10.");
    tb.remover(10);
  
    println("Tabela Hash: \n" + tb);
    println("Tem 10? " + tb.buscar(10));
    println("Tabela Hash: \n" + tb);
    
    
    println("\nTabela Hash: \n" + tb);
    println("Tem 13? " + tb.buscar(13));
    println("Tabela Hash: \n" + tb);
    
    println("\nTabela Hash: \n" + tb);
    println("Remover 13.");
    tb.remover(13);
  
    println("Tabela Hash: \n" + tb);
    println("Tem 13? " + tb.buscar(13));
    

}